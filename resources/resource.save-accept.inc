<?php

function _services_legal_save_accept($data) {
  legal_save_accept($data['version'], $data['revision'], $data['language'], $data['uid']);
  $result = services_legal_connect();
  return $result['accepted'];
}
