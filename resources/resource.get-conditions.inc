<?php

function _services_legal_get_conditions($data = array()) {
  return isset($data['language']) ?
    legal_get_conditions($data['language']) : legal_get_conditions();
}
