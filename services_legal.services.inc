<?php

function services_legal_services_resources() {
  $resources = array(
    'legal' => array(
      'actions' => array(
        'get_conditions' => array(
          'help' => t('Get info about legal conditions.'),
          'file' => array(
            'type' => 'inc',
            'module' => 'services_legal',
            'name' => 'resources/resource.get-conditions',
          ),
          'callback' => '_services_legal_get_conditions',
          'args' => array(),
          'access callback' => '_services_legal_access',
          'access callback file' => array(
            'type' => 'inc',
            'module' => 'services_legal',
            'name' => 'services_legal.services',
          ),
          'access arguments' => array('view Terms and Conditions'),
          'access arguments append' => TRUE,
        ),
        'save_accept' => array(
          'help' => t('Save acceptance of conditions for a user.'),
          'file' => array(
            'type' => 'inc',
            'module' => 'services_legal',
            'name' => 'resources/resource.save-accept',
          ),
          'callback' => '_services_legal_save_accept',
          'args' => array(
            array(
              'name' => 'data',
              'type' => 'array',
              'source' => 'data',
              'optional' => FALSE,
            )
          ),
          'access callback' => '_services_legal_access',
          'access callback file' => array(
            'type' => 'inc',
            'module' => 'services_legal',
            'name' => 'services_legal.services',
          ),
          'access arguments' => array('save accept'),
          'access arguments append' => TRUE,
        )
      ),
    ),
  );
  return $resources;
}

function _services_legal_access($op, $args = array()) {
  switch ($op) {

    // Authenticated users can only accept their own conditions.
    case 'save accept':
      $data = $args[0];
      return user_is_logged_in() && $GLOBALS['user']->uid == $data['uid'];
      break;

    // By default we just run the op through user_access().
    default:
      return user_access($op);
      break;

  }
}
