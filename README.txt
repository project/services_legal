Services Legal

|===========|
| IMPORTANT |
|===========|

Run these terminal commands to patch the Legal module for an issue: https://www.drupal.org/node/2167987

cd sites/all/modules/legal
wget https://www.drupal.org/files/issues/legal-add_exception_path_configuration-2167987-4.patch
patch -p1 < legal-add_exception_path_configuration-2167987-4.patch
drush cc menu

Then in Drupal, go to admin/config/people/legal/settings and have your endpoint excluded by entering something like this
in the text area:

[endpoint]/*

If your endpoint's path was "api", then you'd enter: api/*

|==============|
| HOW IT WORKS |
|==============|

This module appends data to the System Connect resource so an application can determine the user's legal status on the
Drupal site. Since 99.9% of apps make a call to System Connect upon start up, it makes sense to include some context
about the user's current legal status.

Once it's determined the user hasn't agreed to the terms and conditions, this module allows you then to load up the
configuration for the current version of the Terms and Conditions so a front end display can be built out of it.

The module then allows the current user to accept the terms and conditions..

|==================|
| GET LEGAL STATUS |
|==================|

Make a POST call to ?q=[endpoint]/system/connect.json and notice the "legal" object included within the result.

Here's an example result for anonymous users.

// Anonymous user:
{
  "sessid": "abc123",
  "session_name": "xyz456",
  "user": {
    "uid": 0
    /* ... */
  },
  "legal": {
    "tc_id": "1"        // The current Version ID available at admin/config/people/legal
  }
}

Here's an example result for authenticated users.

// Authenticated user:
{
  "sessid": "abc789",
  "session_name": "xyz10",
  "user": {
    "uid": "123"
    /* ... */
  },
  "legal": {
    "tc_id": "1"
    "accepted": false // If false, user has not accepted the current Version ID.
  }
}

Notice user 123 hasn't accepted Version 1 of the Terms and Conditions? An application can make a decision with this to
load Version 1 of the Terms and Conditions and then display it to the user.

|======================|
| GET LEGAL CONDITIONS |
|======================|

Here's an example to load the current Version ID of the Terms and Conditions. Make a POST call to:

?q=[endpoint]api/legal/get_conditions.json

And get back some JSON like this:

{
  "tc_id": "1",
  "version": "1",
  "revision": "1",
  "language": "en"
  "conditions": "<p>You agree to this, or else. Or else what? That's what.</p>",
  "date": "1506454070",
  "extras": {
    "extras-1": "I also agree to this.",
    "extras-2": "Oh, I definitely agree to that.",
    /* ... */
  },
  "changes": "We added this and removed that, but we know you'll never actually read it."
}

With all this data, your front end application can build an interface for them to view and agree to the terms.

|======================|
| ACCEPTING CONDITIONS |
|======================|

Once you have an interface to present the Terms and Conditions, you'll want to allow the user to agree to them. By
design only the current user can accept the terms and conditions for themselves. Make a
POST call to:

?q=[endpoint]/legal/save_accept

With the following payload:

{
  "version": "1",
  "revision": "1",
  "language": "en",
  "uid": "123"
}

And you'll get back either:

[true]
[false]

An array with one element in it, a boolean indicating if it was successful or not.

That's it! Now try to get your users to actually read your terms and conditions without just blindly agreeing to them ;)
